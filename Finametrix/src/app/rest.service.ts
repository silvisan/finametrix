import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  getDatosCriptomoneda(moneda,divisa): Observable<void> {   
    const endpoint = 'https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol='+ moneda +'&market='+ divisa +'&apikey=TTO984XWR62C3Z2D';
    return this.http.get<void>(endpoint,
        {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });
  }
}

