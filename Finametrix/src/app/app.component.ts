import { Component } from '@angular/core';
import { RestService } from './rest.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DatePipe]
})
export class AppComponent {
  monedas = ["BTC", "ETH", "XRP"]; /* top criptomonedas */
  divisa = "EUR"; /* divisa por defecto */
  datos = []; /* datos originales desde la api */
  datos_filtro = []; /* array con las criptomonedas resultantes al buscar */
  click_moneda = ''; /* criptomoneda seleccionada */ 
  orden = ''; /* elemento a ordenar */
  tipo_orden = 'des'; /* tipo de ordenación asc-des */

   /* configuración del gráfico */
  public lineChartData:Array<any> = [];
  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    //responsive: true, 
    elements: 
    { 
        point: 
        {
            radius: 0,
            hitRadius: 5,
            hoverRadius: 10,
            hoverBorderWidth: 2
        }
    }
  };
  public lineChartColors:Array<any> = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
    }  
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  constructor(private restApi: RestService, private datePipe: DatePipe) { 

    this.monedas.forEach(moneda => {
      if(localStorage.getItem(moneda) ){
        let m = JSON.parse(localStorage.getItem(moneda));
        /* Posible solución para la delimitación de la api, que solo consulte a la 
        api si la fecha de última actualización de la criptomoneda es menor que hoy */
       // if(m.Note || this.datePipe.transform(new Date(), 'yyyy-MM-dd') > Object.keys(m["Time Series (Digital Currency Daily)"])[0])
        if(m.Note)
          this.getCriptomoneda(moneda);
        else
          this.datos.push(m);
      }else{
        this.getCriptomoneda(moneda);
      }
    });
    //console.log('datos',this.datos);
    this.datos_filtro = this.datos;
  }

  /* Función que recoge los datos de cada criptomoneda */
  getCriptomoneda(moneda){
      this.restApi.getDatosCriptomoneda(moneda,this.divisa).subscribe(m => {
        if(!m["Note"]){
          this.formatoDatos(m);
          this.datos.push(m);
          localStorage.setItem(moneda,JSON.stringify(m));
        }
      });
  }

  /* Función que da formato a los datos recogidos desde la api */
  formatoDatos(m){
      m.Day = Object.values(m["Time Series (Digital Currency Daily)"])[Object.keys(m["Time Series (Digital Currency Daily)"]).length - 1];
      m.lineChartData = [{ data: [], label: 'Precios de cierre' }];
      Object.values(m["Time Series (Digital Currency Daily)"]).forEach(mts => {
        if(this.divisa == "EUR")
          m.lineChartData[0].data.push(mts["4a. close (EUR)"])
        else
          m.lineChartData[0].data.push(mts["4a. close (USD)"])
      });
      m.lineChartData[0].data = Object.assign([],  m.lineChartData[0].data).reverse();
      m.lineChartLabels =  Object.assign([],Object.keys(m["Time Series (Digital Currency Daily)"])).reverse();
  }

  /* Función que muestra el contenedor con la información de la criptomoneda */
  mostrarDatos(moneda){
    if(moneda == this.click_moneda)
      this.click_moneda = '';
    else
      this.click_moneda = moneda;
  }

  /* Función que busca en la tabla por nombre de criptomoneda */
  buscador(texto){
    this.datos_filtro = [];
    this.datos.forEach(d => {
      if((d["Meta Data"]["2. Digital Currency Code"]).toLowerCase().includes(texto.toLowerCase())){
        this.datos_filtro.push(d);
      }
    });
    this.click_moneda = '';
  }


  /* Función que ordena la tabla asc-des */
  ordenar(valor){
    if(this.orden == valor && this.tipo_orden == 'des'){
      this.tipo_orden = 'asc';
    }else{
      this.orden = valor;
      this.tipo_orden = 'des';
    }

    switch (valor) {
      case 'market':
        if(this.tipo_orden == 'des'){
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["6. market cap (USD)"] < b["Day"]["6. market cap (USD)"]  ? 1 : a["Day"]["6. market cap (USD)"]  > b["Day"]["6. market cap (USD)"]  ? -1 : 0;
          });
        }else{
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["6. market cap (USD)"]  > b["Day"]["6. market cap (USD)"]  ? 1 : a["Day"]["6. market cap (USD)"]  < b["Day"]["6. market cap (USD)"]  ? -1 : 0;
          });
        }
        break;
      case 'close':
        if(this.tipo_orden == 'des'){
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["4a. close (EUR)"] < b["Day"]["4a. close (EUR)"] ? 1 : a["Day"]["4a. close (EUR)"] > b["Day"]["4a. close (EUR)"] ? -1 : 0;
          });
        }else{
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["4a. close (EUR)"] > b["Day"]["4a. close (EUR)"] ? 1 : a["Day"]["4a. close (EUR)"] < b["Day"]["4a. close (EUR)"] ? -1 : 0;
          });
        }
        break;
      case 'volume':
        if(this.tipo_orden == 'des'){
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["5. volume"] < b["Day"]["5. volume"] ? 1 : a["Day"]["5. volume"] > b["Day"]["5. volume"] ? -1 : 0;
          });
        }else{
          this.datos_filtro.sort(function (a, b) {
            return a["Day"]["5. volume"] > b["Day"]["5. volume"] ? 1 : a["Day"]["5. volume"] < b["Day"]["5. volume"] ? -1 : 0;
          });
        }
        break;
      default:
    }
  }

  cambiarDivisa(divisa){
    //console.log(divisa);
    if(this.divisa != divisa){
      //this.divisa = divisa;
      /*this.datos = [];
      this.datos_filtro = [];
      this.monedas.forEach(moneda => {
        this.getCriptomoneda(moneda);
      });*/
      //console.log('datos',this.datos);
    }
  }
}
